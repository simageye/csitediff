# Makefile

# PHONY := target is not associated with a physical file 
# #(z.B.: the target "clean" is independent from the file "clean", if it exists)
.PHONY: clean all build cleanc cleani prepareproject

# Define Variables
PROJOLD:=$(shell grep PROJOLD ./.env | cut -f2 -d"=")

PROJ:=$(shell grep "PROJ=" ./.env | cut -f2 -d"=")

ROOTDIR=./${PROJ}

CONTAINERNAME:=$(shell grep CONTAINERNAME ./.env | cut -f2 -d"=")
IMAGENAME:=$(shell grep IMAGENAME ./.env | cut -f2 -d"=")
VER:=$(shell grep VERSION ./.env | cut -f2 -d"=")


# --- Makefile Logic ---
CHDIR_SHELL := $(SHELL)
define chdir
   $(eval _D=$(firstword $(1) $(@D)))
   $(info $(MAKE): cd $(_D)) $(eval SHELL = cd $(_D); $(CHDIR_SHELL))
endef


all: cleanc cleani  
	$(call chdir,$(ROOTDIR))
	echo "I'm now always in $(ROOTDIR)"
	docker-compose up -d

build: 
	$(call chdir,$(ROOTDIR))
	echo "I'm now always in $(ROOTDIR)"
	docker-compose up -d

clean: cleanc cleani

cleanc:
	docker rm -f ${CONTAINERNAME}

cleani:
	docker rmi ${IMAGENAME}:${VER}

prepareproject: 
	ln -s ./.env ./${PROJ}/.env
	mv ./${PROJOLD}/${PROJOLD} ./${PROJOLD}/${PROJ}
	mv ./${PROJOLD} ${PROJ}
	