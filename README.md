# Alpine k9s Image/Container  
______

# Changing version
Versionnumber can be edited in ".env"
______

______
# Dockerizing
$ docker-compose up -d <br/>
$ docker-compose down <br/>
$ docker-compose down -rmi

### Clean up
$ docker rm $(docker ps -a -q) -f && docker rmi $(docker images -q) -f
______
# Commands until running

#### 1. Download this repository
...
#### 2. stop and delete all ik9s container and imges 
$ docker ps | grep "ik9s" | awk '{print $1}' | xargs docker rm -f
$ docker images -a | grep "ik9s" | awk '{print $3}' | xargs docker rmi
#### 3. Build the image (find the directory where the Makefile is (hint: project root directory))
$ make all

### Starting from image:
$ docker run -v ~/.kube/config:/USER/.kube/config --rm --name ck9s-s  -ti ik9s:VERSION  /usr/local/bin/k9s <br/>
With <Ctr-c>    Exit and delete Container<br/>
With USER = e.g. root<br/>
With VERSION = e.g. 0.7.11<br/>

Was nicht gehen darf:<br/>
$ docker run --rm --name ck9s-s  -ti ik9s-s:0.7.11 ash<br/>
$ docker run --rm --name ck9s-s  -ti ik9s-s:0.7.11 /usr/local/bin/k9s

```Bash
#!/bin/bash

# Main()
function main() {

	if [ "$1" == "start-s" ]
	then	
		docker run -v ~/.kube-s/config:/root/.kube/config --rm --name ck9s-s-$(date +%N)  -ti ik9s:$(docker images ik9s -f "reference=ik9s:*" | grep ik9s | awk '{print $2}') /usr/local/bin/k9s
	elif [ "$1" == "start-p" ]
	then
		docker run -v ~/.kube-p/config:/root/.kube/config --rm --name ck9s-p-$(date +%N)  -ti ik9s:$(docker images ik9s -f "reference=ik9s:*" | grep ik9s | awk '{print $2}')  /usr/local/bin/k9s
	else
		echo "try: $ dk9s start-s"
		echo "OR"
		echo "try: $ dk9s start-p"
	fi
}
main $1
exit;
```
______
### Quellen

# k9s
(https://k9ss.io/) <br />
https://github.com/derailed/k9s/releases

# Understanding docker ARG and ENV
https://vsupalov.com/docker-arg-env-variable-guide/#arg-and-env-availability

______
### Clean dir tree
```Bash
.
├── Makefile
├── README.md
├── cik9s.graphml
└── k9s
    ├── docker-compose.yml
    ├── k9s_k8s_config_rbb_s_kube
    │   └── config
    └── k9s_ubuntu_dockerfile
        └── Dockerfile
```
